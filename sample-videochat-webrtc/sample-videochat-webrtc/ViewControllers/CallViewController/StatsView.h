//
//  StatsView.h
//  BacSiViet-old
//
//  Copyright (c) 2017 QuickBlox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsView : UIView

- (void)setStats:(NSString *)stats;

@end
