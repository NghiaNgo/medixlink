//
//  FilteredUsersViewController.h
//  BacSiViet
//
//  Created by Smisy on 3/24/18.
//  Copyright © 2018 QuickBlox Team. All rights reserved.
//

#import "UsersViewController.h"

@interface FilteredUsersViewController : UsersViewController

@property (nonatomic, strong) NSArray *filteredUsers;

@end
