//
//  RecordsViewController.h
//  BacSiViet
//
//  Created by Andrey Ivanov on 28.02.17.
//  Copyright © 2017 QuickBlox Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordsViewController : UITableViewController

@property (assign, readonly) BOOL playerPresented;

@end
