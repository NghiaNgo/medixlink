//
//  RegisterTableViewController.h
//  BacSiViet
//
//  Created by Smisy on 3/29/18.
//  Copyright © 2018 QuickBlox Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginTableViewController.h"

@interface RegisterTableViewController : UITableViewController

@property (nonatomic, retain) LoginTableViewController *loginTableViewController;

@end
